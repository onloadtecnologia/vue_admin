import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import store from "../store/index.js"

const routes = [
  {
    path: "/",
    name: "Home",
    component: HomeView,
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/LoginView.vue"),
  },
  {
    path: "/sobre",
    name: "Sobre",
    component: () => import("../views/SobreView.vue"),
  },
  {
    path: "/admin/:id?",
    name: "Admin",
    component: () => import("../views/AdminView.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to, from, next) => {
  if(to.name=="Admin" && !store.getters["logado"])
  {
    // console.log(store.getters["logado"])
    next({name:"Login"})
  }
  next();
  

});

export default router;
