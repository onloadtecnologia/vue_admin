const links = [
    {
      "to": "/",
      "title": "Home",
      "show":true
    },
    {
      "to": "/sobre",
      "title": "Sobre",
      "show":true
    },
    {
      "to": "/login",
      "title": "Login",
      "show":true
    },
    {
      "to": "/admin/",
      "title": "Admin",
      "show":false
    }
 ];

 export default links;