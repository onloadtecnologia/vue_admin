import { createStore } from 'vuex'
import Swal from 'sweetalert2'

export default createStore({
  state: {
    dados:{
      username:null,
      email:null,
      logado:false
    }
  },
  getters: {
    logado(state){
      return state.dados.logado;
    }
  },
  mutations: {
    login(state,payload){
      if(payload.email == "alan@gmail.com" && payload.password == 123){
      state.dados.username=payload.username
      state.dados.email=payload.email
      state.dados.logado=true
      Swal.fire({
        title:"Bem-Vindo!",
        icon:"success",
        text:`Olá Sr(a) ${state.dados.username}`
      })
      }else{
        state.dados.username=null
        state.dados.email=null
        state.dados.logado=false
      }
      return state.dados.logado;     
    },
    logout(state){
      state.dados.username=null
      state.dados.email=null
      state.dados.logado=false
      window.location = "/"
    }
  },
  actions: {
  },
  modules: {
  }
})
